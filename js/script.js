"use strict";

const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function getList(arr, parent = body) {
  const list = arr.map((element) => `<li>${element}</li>`);
  const printLi = list.join("");

  const printUl = document.createElement("ul");

  printUl.innerHTML = printLi;

  document.querySelector(parent).append(printUl);
}

getList(arr, ".list");
